FROM node:argon
ADD . /app
COPY package.json /app
WORKDIR /app
RUN npm install -g sails && npm install sails
RUN npm install
CMD npm start  
EXPOSE 8080