# Auction Test Application

## Sails + Webpack + React + Socket.IO

### Starting application

If you using docker, you can run app over the docker container by:

```bash
    docker-compose up
```
or run with npm:

```bash
    npm install
    npm start
```
### DynamoDB configuration 
run local server 
```bash
    java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb 
```

if you will have some issues with dynamoDb you can change adapter to localDiskDb:

```javascript

config/models.js
connection: dynamoDb, //'localDiskDb'

```

### Login as Admin

You can login as Admin, and you can see RealTime data,
for todo this you should create profile with email admin@admin.com
also u can change this email from the sailsjs config

```bash
    config/globals.js

    adminEmail: 'admin@admin.com'
```

