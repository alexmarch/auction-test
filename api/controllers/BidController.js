/**
 * BidController
 *
 * @description :: Server-side logic for managing bids
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const _ = require('lodash');
const Provider = require('../db/provider').Provider;
const dbInstance = new Provider();
const bidModel = dbInstance.model('Bid');

module.exports = {
    //Creaate bid
    create(req, res) {
        if (req.body.bid) {
            var bid = {
                name: req.body.bid.name,
                amount: req.body.bid.amount,
                owner: {
                    email: req.body.user.email,
                    company: req.body.user.company
                }
            };
            bidModel.create(bid, (err, row) => {
                if (!err) {
                    AdminService.broadcastToAdmin(req._sails.sockets);
                    return res.json({ result: 'success', data: row.attrs });
                }
                res.json({ result: err });
            });
        } else {
            return res.badRequest()
        }
    },
    //Update bid
    update(req, res) {
        if (req.body.bid) {
            bidModel.update({ id: req.param('id'), amount: req.body.bid.amount }, (err, result) => {
                if (!err) {
                    AdminService.broadcastToAdmin(req._sails.sockets);
                    return res.json({ result: 'success', data: result.attrs });
                }
                res.json({ result: 'error' });
            });
        } else {
            return res.badRequest()
        }
    },
    //Only admin can view auction look on policies rules
    viewAuctions(req, res) {
        if (!req.isSocket) return res.badRequest();
        req._sails.sockets.join(req, 'auction');
        AdminService.broadcastToAdmin(req._sails.sockets);
        return res.ok();
    },
    syncTime(req, res) {
        if (!req.isSocket) return res.badRequest();
        req._sails.sockets.join(req, 'auction');
        // AdminService.broadcastToAdmin(req._sails.sockets);
        return res.ok();
    },
    //After auction time finish
    getAuctionResult(req, res) {
        if (!req.body.bids) return res.badRequest();
        var bids = [];
        req.body.bids.forEach(bid=>{
            bids.push(bid.name);
        });
        bidModel.scan()
            .where('name').in(bids)
            .exec((err, result) => {
                if( !err && result ) {
                    var resultBids = _.map(result.Items, 'attrs');
                    resultBids.forEach(bid=>{
                       var b = _.find(req.body.bids, { name: bid.name });
                       if( b.amount >= bid.amount ){
                           b.success = true;
                       }else{
                           b.success = false;
                       }
                    });
                    return res.json({bids: req.body.bids});
                }
                res.json({bids: []});
            })

    }
};

