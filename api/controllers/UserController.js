'use strict';
/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const Provider = require('../db/provider').Provider;
const dbInstance = new Provider();
const modelUser = dbInstance.model('User');

// Helpers
const isAdmin = (email) => { 
    return email === sails.config.globals.adminEmail 
};
const resUser = (req, res, user) => {
    if (isAdmin(user.email)) {
        user.type = 'admin'
    } else {
        user.type = 'user';
    }
    req.session.user = { email: user.email, type: user.type };
    res.json({ user: user });
}
// Controller actions
module.exports = {
    // Create new user profile
	create(req, res){
        if (req.body.user) {
            var acc = req.body.user;
            modelUser.get(acc.email, {ConsistentRead: true}, (err, user)=>{
                if (err) return res.json({error: err});
                if (user) {
                    return resUser(req, res, user.attrs);
                }
                modelUser.create({
                    name: acc.name, 
                    company: acc.company,
                    phone: acc.phone,
                    email: acc.email 
                }, (err, user)=>{
                    if(err) return res.json({error: err});
                    return resUser(req, res, user.attrs);
                })
            });
        } else {
            return res.badRequest()
        }
    },
    //Check session state
    sessionState(req, res){
        if(!req.session.user) return res.json({'state':'none'});
        res.json({'state':'active'});
    }
};

