const Joi = require('joi');
const vogels = require('vogels');
module.exports = {
    hashKey: 'id',
    //rangeKey: 'updateAt',
    timestamps: true,
    schema: {
        id: vogels.types.uuid(),
        name: Joi.string().required(),
        amount: Joi.number().required(),
        owner: Joi.object().keys({
            email: Joi.string(),
            company: Joi.string()
        })
    }
}