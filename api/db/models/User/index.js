const Joi = require('joi');
module.exports = {
    hashKey: 'email',
    timestamps: true,
    schema: {
        email:  Joi.string().email(),
        name:   Joi.string(),
        company: Joi.string(),
        phone: Joi.string(),
    }
}