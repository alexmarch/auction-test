'use strict';
const vogels = require('vogels');
const models = require('../models');
const _ = require('lodash');

class Provider {
    constructor(){
        vogels.AWS.config.update(require('../../../config/dynamodb'));
    }
    //Get model by name
    model(name){
        return vogels.define(_.lowerCase(name), models[name]);
    }
    deleteTables(cb){
         // Define all models
        _.each(models, (v, k)=>{
            var table = vogels.define(_.lowerCase(k), v);
            table.deleteTable(function(err) {
                if(!err){
                    console.log(`Table ${k} has been deleted`);
                }
            }); 
        });
        cb();

    }
    createTables(){
        // Define all models
        _.each(models, (v, k)=>{
            vogels.define(_.lowerCase(k), v);
        });
        // Create tables
        vogels.createTables((err)=>{
            if (!err) {
                console.log(`Tables has been created`);
            }
        });
    }
}module.exports.Provider = Provider;