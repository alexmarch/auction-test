/**
 * Bid.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    amount: {
      type: 'float',
      required: true
    },
    bidOwner: {
      model: 'user'
    }
  },
  broadcastToAdmin(sockets){
     Bid.find({sort: 'updatedAt DESC'}).populate('bidOwner').exec((err, results)=>{
        if(!err) sockets.broadcast('auction', 'bids', results);
     });
  }
};

