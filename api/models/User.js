/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'users',
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    company: {
      type: 'string',
      required: true
    },
    phone: {
      type: 'string',
      required: true
    },
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    bids: {
      collection: 'bid',
      via: 'bidOwner'
    }
  }
};

