module.exports = (req, res, next) => {
    if(!req.session.user){
        return res.badRequest();
    }
    if(req.session.user.type === 'admin'){
        return next();
    } else {
        return res.forbidden('Allowed only for admin user !');
    }
}