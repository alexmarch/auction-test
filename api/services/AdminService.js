const _ = require('lodash');
const Provider = require('../db/provider').Provider;
const dbInstance = new Provider();
const bidModel = dbInstance.model('Bid');

module.exports = {
    broadcastToAdmin(sockets){
        bidModel.scan().exec((err, bids)=>{
            if(!err) {
                var bids = _.orderBy(_.map(bids.Items, 'attrs'), ['createdAt', 'updatedAt'], ['desc','desc']);
                sockets.broadcast('auction', 'bids', bids);
            }
        });
    }
}