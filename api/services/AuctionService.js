'use strict';

let interval = 1000;
const maxMinutes = 5;
let minutes=0, seconds=0;
let minutesLeft=4;
let secondLeft=60;

module.exports = {
    start(){
        this.intervalHandler = setInterval(()=>{
            if(seconds++ === 60){
                minutes++;
                seconds = 0;
                minutesLeft--;
                //if(minutesLeft === 2){
                // this.stop();
                // }
            }
            if(secondLeft-- === 0){
                secondLeft = 60;
            }
            if(minutes >= maxMinutes){
                clearInterval(this.intervalHandler);
                this.stop();
            }
            this.broadcast();
        }, interval);
    },
    stop(){
        seconds = minutes = 0;
        minutesLeft = 5;
        secondLeft = 60;
        clearInterval(this.intervalHandler);
        sails.sockets.broadcast('auction', 'stopAuction');
    },
    broadcast(){
        sails.sockets.broadcast('auction', 'synctime', {minutesLeft: minutesLeft, secondLeft: secondLeft});
    }
}