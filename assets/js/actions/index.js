import Reflux from 'reflux';

const AuctionActions = Reflux.createActions([
    'startAuction',
    'stopAuction',
    'subscribeBids',
    'getAuctionResult'
]);

const UserActions = Reflux.createActions([
    'createUser',
    'logoutUser',
    'sendBid',
    'updateBid'
]);

export {UserActions, AuctionActions};