import axios from 'axios';
import {UserActions} from '../../actions';

const auth =  {
    isAuth(cb){
        this.sessionState().then((res)=>{
            if( res.data.state === 'none' ){
                localStorage.removeItem('user');
                UserActions.logoutUser();
            }
            try{
                if(localStorage.getItem('user') !== undefined) {
                    cb(JSON.parse(localStorage.getItem('user')));
                }
            } catch(e) {}
            return cb(null);
        })
    },
    getUser(){
        return JSON.parse(localStorage.getItem('user'));
    },
    logout(){
        localStorage.removeItem('user');
    },
    authUser(user){
        localStorage.setItem('user', JSON.stringify(user));
    },
    sessionState(){
        return axios.get('/user/session/state');
    },
}
export default auth;