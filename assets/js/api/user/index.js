import axios from 'axios';

const user = {
    getCSRF(){
        return axios.get('/csrfToken');
    },
    create(user){
        return axios.post('/user/create', {user});
    },
    getBids(){
        return [
            {name: 'Dial1', amount: undefined },
            {name: 'Dial2', amount: undefined },
            {name: 'Dial3', amount: undefined },
            {name: 'Dial4', amount: undefined },
            {name: 'Dial5', amount: undefined }
        ]
    },
    sendBid(bid, user){
        return axios.post('/bid/create', {bid, user});
    },
    updateBid(bid, user){
        return axios.put(`/bid/update/${bid.id}`, {bid, user});
    },
    getAuctionResult(bids){
        return axios.post('/auction/result', {bids});
    }

};

export default user;