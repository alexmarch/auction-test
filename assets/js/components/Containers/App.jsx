
import React, {Component} from 'react';
import Navbar from '../Navbar/Navbar.jsx';
import './app.less';

export default class App extends Component {
    constructor(props){
        super(props);
        
    }
    componentWillMount(){
        
    }
    render(){
        return (
            <div className="container-fluid">
                <Navbar {...this.props} />
                <div className="container">
                   {/* <CreateAccount /> */ }
                   { this.props.children }
                </div>
            </div>
        );
    }
}