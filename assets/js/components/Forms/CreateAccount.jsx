import React, {Component} from 'react';
import { FormGroup, ControlLabel, FormControl, Button, Panel } from 'react-bootstrap';
import { UserActions } from '../../actions';

import './forms.less';

export default class CreateAccount extends Component {
    state = {
        name: '',
        company: '',
        phone: '',
        email: ''
    }
    constructor(props){
        super(props);
    }
    nameChangeHandler(event){
        this.setState( { name: event.target.value });
    }
    handleCompanyChange(event){
        this.setState( { company: event.target.value });
    }
    handlePhoneChange(event){
        this.setState( { phone: event.target.value });
    }
    handleEmailChange(event){
        this.setState( { email: event.target.value });
    }
    onSubmit(e){
        e.preventDefault();
        UserActions.createUser(this.state);
        return true;
    }
    getValidationStateName(){
    }
    render(){
        const formFooterStyle = {
            textAlign: 'right'
        }
        return (
            <Panel className="basic-form" header="Register new account">
                <form onSubmit={this.onSubmit.bind(this)}>
                    <FormGroup validationState={this.getValidationStateName()}>
                            <ControlLabel>Name</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.name}
                                onChange={this.nameChangeHandler.bind(this)} />
                        
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Company Name</ControlLabel>
                        <FormControl
                                type="text"
                                value={this.state.company}
                                onChange={this.handleCompanyChange.bind(this)} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Phone Number</ControlLabel>
                        <FormControl
                                type="number"
                                value={this.state.phone}
                                onChange={this.handlePhoneChange.bind(this)} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                                type="email"
                                value={this.state.email}
                                onChange={this.handleEmailChange.bind(this)} />
                    </FormGroup>
                    <FormGroup style={formFooterStyle}>
                        <Button bsStyle="success" className="btn-block" bsSize="large" type="submit">Start!</Button>
                    </FormGroup>
                </form>
            </Panel>
        );
    }
}