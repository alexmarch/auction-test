import React, {Component} from 'react';
import auth from '../../api/auth';
import {UserActions} from '../../actions';
import AuctionStore from '../../stores/Auction.js';

import './navbar.less';

const RigthNavBar = (props) => {
   return ( auth.getUser() && <div>
            Welcome { auth.getUser().name }
            <a className="logout" onClick={()=>{ props.logout() }} href="javascript:void(0)">Logout</a>
            {props.minutes > 0 && props.isRunning ? 
                (
                    <div>
                        <span>
                            Auction Closes in:&nbsp;
                            <span className="auction-time">
                                { props.minutes >= 10 ? props.minutes : '0' + props.minutes }:{ props.seconds >= 10 ? props.seconds : '0' + props.seconds }
                            </span>
                        </span>
                    </div> 
                ) : null }
          </div>
   );
}
export default class Navbar extends Component {
    state = {
        minutes: 0,
        seconds: 0,
        isRunning: true
    }
    constructor(props){
        super(props);
        AuctionStore.listen(this.auctionActions.bind(this));
    }
    auctionActions(action){
        if(action.event === 'stop'){
            action.isRunning = false;
        }
        if(action.event === 'start'){
            action.isRunning = true;
        }
        this.setState(action);
    }
    logout(){
        UserActions.logoutUser();
    }
    render(){
        return (
                <nav className="navbar navbar-default navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">
                                { /*<img alt="Brand" />*/ }
                                Auction
                            </a>
                         
                        </div>
                        <div className="navbar-text navbar-right">
                               { auth.getUser() && <RigthNavBar minutes={this.state.minutes} seconds={this.state.seconds} logout={this.logout.bind(this)} isRunning={this.state.isRunning} /> }
                        </div>
                    </div>
                </nav>
        )
    }
}