const UserConstants = {
    TYPE: {
        ADMIN: 'admin',
        USER: 'user'
    }
};
export {UserConstants};