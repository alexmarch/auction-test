import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory, hashHistory } from 'react-router';
import UserStore from './stores/User.js';
import AuctionStore from './stores/Auction.js';

import 'bootstrap/dist/css/bootstrap.css';
import routes from './routes';

ReactDOM.render(
    <Router routes={routes} history={hashHistory} />, 
    document.querySelector('#app'));