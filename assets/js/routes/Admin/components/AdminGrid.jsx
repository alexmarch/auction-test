import React, {Component} from 'react';
import {Table, Panel} from 'react-bootstrap';
import {AuctionActions, UserActions} from '../../../actions';
import AuctionStore from '../../../stores/Auction.js';

import './admin-grid.less'

class AdminGrid extends Component {
    state = {
        bids: []
    }
    constructor(props){
        super(props);
        AuctionStore.listen(this.onAuctionActions.bind(this));
        AuctionActions.startAuction();
        AuctionActions.subscribeBids();
    }
    getDate(date) {
        return new Date(date).toLocaleDateString() + ' - ' + new Date(date).toLocaleTimeString();
    }
    onAuctionActions(e){
        //console.log('auction event', e);
        if(e.event === 'bids'){
            console.log('bids', e.data);
            this.setState({ bids: e.data });
        }
    }
    render(){
         return ( 
             <Panel className="realtime-ticker" header="Realtime Ticker">
                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>Deal</th>
                            <th>Company Name</th>
                            <th>Bid $</th>
                            <th>Country</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.bids.length > 0 && this.state.bids.map((bid, idx)=>{
                            return (
                                <tr key={'row'+idx}>
                                    <td>{bid.name}</td>
                                    <td>{bid.owner.company}</td>
                                    <td>{bid.amount}</td>
                                    <td>---</td>
                                    <td>{ this.getDate(bid.updatedAt || bid.createdAt) }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </Panel>
         );
    }
} module.exports = AdminGrid;