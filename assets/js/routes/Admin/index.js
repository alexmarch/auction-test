module.exports = {
    path: '/admin',
    getComponent(nextState, cb){
        require.ensure([],(require)=>{
            cb(null, require('./components/AdminGrid'));
        });
    }
}