import React, {Component} from 'react';
import { FormGroup, InputGroup, FormControl, Button} from 'react-bootstrap';
import './bids-form.less';

const ResultMsgOK = (props) =>{
    return <div className="text-success">You WON this Deal $ {props.amount} !</div>
}
const ResultMesage = (props) => {
    return props.isSuccess ? <ResultMsgOK amount={props.amount} /> : <div className="text-danger">You lost this Deal!</div>
}
export default class Bid extends Component {
    onChangeBid(e){
        this.props.item.amount = e.target.value;
        if(this.props.item.submitted){
            this.props.item.submitted = false;
        }
        this.forceUpdate();
    }
    getValidationState(){
        if(!isNaN(parseFloat(this.props.item.amount))){
            return 'success'
        } 
    }
    render(){
        return(
            <div className="bid">
                <div className="row">
                    <div className="col-md-3">
                        <FormGroup validationState={this.getValidationState()}>
                            <label>{this.props.item.name}</label>
                            <InputGroup>
                                <span className="input-group-addon" id="basic-addon1">$</span>
                                <FormControl disabled={this.props.isState === 'stop'} type="number" onChange={this.onChangeBid.bind(this)} />
                            </InputGroup>
                        </FormGroup>
                    </div>
                    <div className="col-md-4 submit-btn">
                            {(!this.props.item.submitted && this.props.isState === 'running') ? 
                            <Button disabled={this.props.isState === 'stop'} bsStyle="default" onClick={()=>{
                                this.props.onSubmit(this.props.item);
                            }}>
                                Submit Bid
                            </Button> : 
                            (this.props.isState === 'result' ? 
                                <ResultMesage 
                                    isSuccess={this.props.item.success} 
                                    amount={this.props.item.amount} /> :  
                                <div className="bg-success text-success">
                                    Bid submitted succesfully
                                </div>
                            )}
                    </div>
                </div>
            </div>
        )
    }
}