import React, {Component} from 'react';
import { FormGroup, ControlLabel, FormControl, Button, Panel, Modal } from 'react-bootstrap';
import Bid from './Bid.jsx';
import user from '../../../api/user';
import {AuctionActions, UserActions} from '../../../actions';
import AuctionStore from '../../../stores/Auction.js';
import UserStore from '../../../stores/User.js';
import _ from 'lodash';
import './bids-form.less';

class BidsForm extends Component {
    state = {
        bids: user.getBids(),
        isState: 'running',
        currentBid: {}
    }
    constructor(props){
        super(props);
        AuctionActions.startAuction();
        AuctionStore.listen(this.auctionActions.bind(this));
        UserStore.listen(this.onBidSubmit.bind(this));
    }
    onSubmit(bid){
        
        if(bid.amount !== undefined && bid.amount > 0){
            this.setState({show: true, currentBid: bid});
        }
    }
    closeModal(){
        this.setState({show: false});
    }
    sendBid(){
        this.setState({show: false});
        if(this.state.currentBid.id === undefined){
            UserActions.sendBid(this.state.currentBid);
        }else{
            UserActions.updateBid(this.state.currentBid);
        }
    }
    onBidSubmit(action){
        if(action && action.status === 'success'){
            this.state.currentBid.submitted = true;
            this.state.currentBid.id = action.bid.id;
            this.setState({currentBid: this.state.currentBid});
        }
    }
    auctionActions(action){
        if(action.event === 'stop'){
            let submitted = [];
            this.state.bids.forEach(bid=>{
                if(bid.amount > 0 && bid.submitted){
                    submitted.push(bid);
                }
            });
            this.setState({isState: 'stop'});
            AuctionActions.getAuctionResult(submitted);
        } else if (action.event === 'result'){
            action.data.forEach(bid=>{
                let b = _.find(this.state.bids, { name: bid.name });
                b.success = bid.success;
            });
            this.setState({ bids: this.state.bids, isState: 'result' });
        }

    }
    render(){
        return (
            <Panel className="bids-panel" header="List of Bids">
                {this.state.bids && this.state.bids.map((bid,key)=>{
                    return <Bid key={key} item={bid} isState={this.state.isState} onSubmit={this.onSubmit.bind(this)} />
                })}
                { this.state.bids.length === 0 ? <div className="auction-finished">Auction finished !</div> : null }
                <Modal
                    show={this.state.show}
                    onHide={this.closeModal}
                    container={this}
                    aria-labelledby="contained-modal-title"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">Administrator</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Are you sure you would like to submit the bid ?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.sendBid.bind(this)}>Yes</Button>
                        <Button onClick={this.closeModal.bind(this)}>No</Button>
                    </Modal.Footer>
                </Modal>
            </Panel>
        );
    }
}

module.exports = BidsForm;