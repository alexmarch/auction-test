import auth from '../../api/auth';

module.exports = {
  path: 'bids',
  onEnter(nextState, replace){
    auth.isAuth(user=>{
      if(!user){
        replace('/');
      }
    });
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/BidsForm'));
    })
  }
};