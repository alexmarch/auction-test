import App from '../components/Containers/App.jsx';
import { CreateAccount } from '../components/Forms';
import auth from '../api/auth';
import {UserConstants} from '../constants';

export default {
  path: '/',
  component: App,
  indexRoute: { component: CreateAccount },
  onEnter: (nextState, replace) => {
    auth.isAuth(user=>{
      if (!user && nextState.location.pathname !== '/') {
        replace('/');
      } else if (user && nextState.location.pathname === '/') {
        if (user.type === UserConstants.TYPE.USER) {
          replace('/bids');
        } else {
          replace('/admin');
        }
      }
    });
  },
  childRoutes: [
    require('./Bids'),
    require('./Admin')
  ]
}
