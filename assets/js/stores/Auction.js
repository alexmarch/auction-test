import Reflux, {createStore} from 'reflux';
import {AuctionActions} from '../actions';
import user from '../api/user';
import auth from '../api/auth';
import {hashHistory} from 'react-router';
import {UserActions} from '../actions';

let interval = 1000;
const maxMinutes = 5;
let minutes=0, seconds=0;
let minutesLeft=4;
let secondLeft=60;

const AuctionStore = createStore({
    listenables: [AuctionActions],
    init(){
        this.listenTo(UserActions.logoutUser, this.stopAuction);

        this.auctionSocket = io.sails.connect();

        this.auctionSocket.on('synctime', (data)=>{
            minutesLeft = data.minutesLeft;
            secondLeft = data.secondLeft;
        });

        this.auctionSocket.on('stopAuction', (data)=>{
            this.stopAuction();
        });
        
        this.auctionSocket.get('/auction/synctime');
    },
    startAuction(){
        this.trigger({ event : 'start' });
        this.intervalHandler = setInterval(()=>{
            if(seconds++ === 60){
                minutes++;
                seconds = 0;
                minutesLeft--;
            }
            if(secondLeft-- === 0){
                secondLeft = 60;
            }
            if(minutes >= maxMinutes){
                clearInterval(this.intervalHandler);
                this.stopAuction();
            }
            this.trigger({ event: 'update', minutes: minutesLeft, seconds: secondLeft})
        }, interval);
        
    },
    subscribeBids(){
        // this.auctionSocket.on('connect', function onConnect () {
        //   console.log("Socket connected!");
        // });
        this.auctionSocket.on('bids', (data)=>{
            this.trigger({ event: 'bids', data });
        });
        this.auctionSocket.get('/auction/view');
    },
    getAuctionResult(bids){
        user.getAuctionResult(bids).then(res=>{
            if(res.data.bids){
                this.trigger({ event: 'result', data: res.data.bids });
            }
        });
    },
    stopAuction(){
        seconds = minutes = 0;
        minutesLeft = 5;
        secondLeft = 60;
        clearInterval(this.intervalHandler);
        this.trigger({ event : 'stop' });
    }
});

export default AuctionStore;