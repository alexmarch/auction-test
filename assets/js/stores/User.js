import Reflux, {createStore} from 'reflux';
import {UserActions, AuctionActions} from '../actions';
import user from '../api/user';
import auth from '../api/auth';
import { hashHistory } from 'react-router';

const UserStore = createStore({
    listenables: [UserActions],
    createUser(data){
        user.create({...data}).then(res=>{
            if(res.data.user){
                auth.authUser(res.data.user);
                this.trigger({...res.data.user});
                if(res.data.user.type === 'user'){
                    hashHistory.push('bids');
                }else{
                    hashHistory.push('admin');
                }
            }
        });
    },
    logoutUser(){
        auth.logout();
        setTimeout(()=>{
            this.trigger({status: 'logout'});
            hashHistory.push('/');
        }, 200);
    },
    sendBid(bid){
        const cUser = auth.getUser();
        user.sendBid(bid, cUser).then(res=>{
            if(res.data.result === 'success'){
                this.trigger({status: 'success', bid: res.data.data});
            }
        })
    },
    updateBid(bid){
        const cUser = auth.getUser();
        user.updateBid(bid, cUser).then(res=>{
            if(res.data.result === 'success'){
                this.trigger({status: 'success', bid: res.data.data});
            }
        })
    }
});

export default UserStore;