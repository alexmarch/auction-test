const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: [ path.resolve(__dirname, "../app/index.jsx") ]
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    publicPath: "/",
    filename: "[name].bundle.js"
  },
  resolve: {
    extensions: ['', '.dev.js', '.prod.js', '.web.js', '.jsx', '.js']
  },
  module: {
    loaders: [
      {
        test: /\.(jsx|js)$/, 
        loaders: ['react-hot', 'babel?presets[]=stage-0&presets[]=react&presets[]=es2015&plugins[]=transform-react-jsx&plugins[]=transform-es2015-destructuring&plugins[]=transform-class-properties'], 
        exclude: /(node_modules|bower_components)/ 
      },
      {
        test: /\.sass$/, 
        loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader") 
      },
      {test: /\.json?$/, loader: 'json-loader'},
      {test: /\.css$/,  loader: "style-loader!css-loader" },
      {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, "../app/index.html")
    }),
    
    new ExtractTextPlugin("app.css"),

    new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('prod')
      }
    }),

    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, '../app/data'),
      to: path.resolve(__dirname, '../dist/data')
    }]),

    new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
    })
  ]
};