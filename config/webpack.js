const config = require(`./webpack/webpack.${process.env.NODE_ENV ? process.env.NODE_ENV : 'development' }.js`);
module.exports.webpack = { 
  options: config, 
  watchOptions: {
    aggregateTimeout: 300
  } 
};