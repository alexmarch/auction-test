const path = require("path");
const webpack = require("webpack");
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    app: [
        path.resolve(__dirname, "../../assets/js/index.jsx"),
        'webpack-hot-middleware/client?reload=false&overlay=false' 
    ]
  },
  output: {
    path: path.resolve(__dirname, "../../.tmp/public/js"),
    publicPath: '/js',
    filename: "[name].bundle.js"
  },
  resolve: {
    extensions: ['', '.dev.js', '.prod.js', '.web.js', '.jsx', '.js', '.json'],
  },
  module: {
    loaders: [
      { test: /\.json$/, loader: 'json-loader', include: path.resolve(__dirname, "../app/data") },
      {
        test: /\.(jsx|js)$/, 
        loaders: ['react-hot', 'babel?presets[]=stage-0&presets[]=react&presets[]=es2015&plugins[]=transform-react-jsx&plugins[]=transform-es2015-destructuring&plugins[]=transform-class-properties'], 
        exclude: /(node_modules|bower_components)/ 
      },
      {
        test: /\.sass$/, 
        loaders: ["style", "css", "sass"] 
      },
      {
        test: /\.less$/,
        loader: "style!css!less"
      },
      {test: /\.css$/,  loader: "style-loader!css-loader" },
      {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /css?family?/, loader: 'file' },
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
    ]
  },
  devtool: 'eval',
  plugins: [
    //new webpack.optimize.OccurenceOrderPlugin(),
    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, "../../assets/js/dependencies"), 
      to: path.resolve(__dirname, "../../.tmp/public/js/dependencies"),
    }]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  debug: true
};