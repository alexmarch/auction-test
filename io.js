const io = require('sails.io.js')( require('socket.io-client') );

io.socket.on('connect', ()=>{
    //   io.socket.get('/messages');
    //   io.socket.get('/notifications/subscribe/statusUpdates');
    console.log('Socket connected...');
});

io.socket.on('disconnect', ()=>{
      console.log('Lost connection to server');
});

module.exports = io;